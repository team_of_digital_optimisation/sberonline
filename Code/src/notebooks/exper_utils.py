import sys
from pathlib import Path


def add_project_path() -> bool:
    """
    Функция для добавления src в PYTHON_PATH
    """
    project_path = Path('.')
    cur_path = Path(project_path.absolute())
    for parent in cur_path.parents:
        if 'Pipfile' in [obj.name for obj in parent.glob('*')]:
            project_path = Path(parent.absolute())
            break
    src_path = project_path.joinpath('Code/src')
    if project_path == '.':
        return False
    for path in [src_path]:
        if path not in sys.path:
            sys.path.append(str(path.absolute()))
    return project_path
