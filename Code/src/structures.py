import json
from dataclasses import dataclass, field
from typing import Optional, Dict, Union, Set


@dataclass
class Review:
    rating: int
    title: str = ''
    review: str = ''
    answer: Optional[str] = None

    def to_dict(self):
        return self.__dict__

    @classmethod
    def from_dict(cls, new_dict: Dict[str, Union[str, int]]) -> 'Review':
        requirements = 'rating'
        if requirements not in set(new_dict.keys()):
            message = f'dict contain too less keys: {new_dict.keys()}'
            raise AttributeError(message)
        return cls(**new_dict)

    @classmethod
    def from_json(cls, new_json) -> 'Review':
        new_dict = json.loads(new_json)
        return cls.from_dict(new_dict=new_dict)
