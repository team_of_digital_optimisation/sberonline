import datetime
import json
from typing import Dict

from flask import Flask, request, abort

from analyzer.semantic_definer import get_semantic
from analyzer.speller import spell
from analyzer.team_definer import get_team_from_review
from structures import Review
from web_services.utils import get_port

start_time = datetime.datetime.now()
app = Flask(__name__)


@app.route("/")
def handle_root():
    score = '< оценка >'
    title = '< заголовок отзыва >'
    review = '<текст отзыва>'
    answer = '<ответ, если он есть, если его нет, можно пропустить аргумент>'
    for_new_review = f"/new_review?rating={score}&title={title}&review={review}&answer={answer}"
    for_speller = f"/spell_text?text=<текст>"
    return (f"<hr align=\"left\" width=\"300\" size=\"4\" color=\"#ff9900\" />"
            f"что бы проанализировать новый запрос постройте запрос по шаблону:<br>{for_new_review}<br>"
            f"<hr align=\"left\" width=\"300\" size=\"4\" color=\"#ff9900\" />"
            f"что бы использовать исправление опечаток постройте запрос по шаблону:<br>{for_speller}<br>")


@app.route("/new_review", methods=['POST', 'GET'])
def new_review():
    r"""Метод для отправки нового отзыва с названием, заголовком и оценков
    Если это POST запрос, то будет ждать вложенных json вида:
    "{'rating': '<оценка>', 'title': '<заголовок отзыва>', 'review': '<текс отзыва>',
     'answer': '<ответ, если он есть>'}"

     Если это GET запрос, будет ждать заполненные аргументы:
     'rating', 'title', 'review', 'answer'
    """

    if request.method == 'POST':
        data = request.get_json()
        review = Review.from_json(data)
    elif request.method == 'GET':
        data = dict(request.args)
        review = Review.from_dict(data)
    else:
        message = "This request look wild. I really have no idea what I need to answer"
        abort(404, message)
        raise KeyError(message)
    positive = get_semantic(review=review)
    teams = get_team_from_review(review=review)
    return json.dumps({'positive_review': str(positive), 'addressee_teams': list(teams)},
                      ensure_ascii=False).encode('utf8')


@app.route('/spell_text', methods=['POST', 'GET'])  # GET requests will be blocked
def get_spelling_text():
    out_data: Dict[str, str] = dict()
    text: str = ''
    if request.method == 'POST':
        data = request.get_json()
        text = data["text"]
    elif request.method == 'GET':
        text = request.args.get('text')
    if text in ['', None]:
        abort(404, 'There no text or empty text')
        raise AttributeError('there are no text in request')
    out_data['text'] = text
    out_data["spelling_text"] = spell(out_data['text'])
    new_data = json.dumps(out_data, ensure_ascii=False)
    return new_data


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=get_port(), debug=True)
