import re

from pyaspeller import YandexSpeller


speller = YandexSpeller()


def spell(text: str) -> str:
    r"""Функция для исправления ошибок в text"""
    try:
        changes = {change['word']: change['s'][0] for change in speller.spell(text)}
        for word, suggestion in changes.items():
            text = text.replace(word, suggestion)
        return text
    except:
        return text


def text_handler(text):
    text = re.sub('[-/]', ' ', text.lower())
    text = re.sub('[!@#$(),?.«»:\'"\d+]', '', text)
    text = re.sub(r'_+', ' ', text)
    text = re.sub(r'\s+', ' ', text)
    return text
