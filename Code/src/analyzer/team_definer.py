from typing import Set

from analyzer.speller import spell
from loader.constants import KP
from structures import Review


def get_team_from_review(review: Review) -> Set[str]:
    """
    :param review:
    :return: возвращает список отделов, которых касается данный отзыв
    """
    spelled_text = spell(review.review + review.title)
    command = set(KP.extract_keywords(spelled_text))

    return command

